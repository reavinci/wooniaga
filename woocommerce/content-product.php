<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */
defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || false === wc_get_loop_product_visibility( $product->get_id() ) || ! $product->is_visible() ) {
	return;
}
?>
<div class="flex-item">
    <div <?php wc_product_class( 'rt-product', $product ); ?>>
        <div class="rt-product__thumbnail rt-img rt-img--full">
            <?php the_post_thumbnail() ?>
        </div>

        <h3 class="rt-product__title">
            <a href="<?php the_permalink()?>"><?php echo get_the_title() ?></a>
        </h3>

        <?php if ($price_html = $product->get_price_html()): ?>
            <span class="rt-product__price"><?php echo $price_html; ?></span>
        <?php endif;?>
        
    </div>
</div>