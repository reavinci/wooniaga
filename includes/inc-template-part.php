<?php

function rt_header(){
    rt_get_template_part('header/primary-menu');
}
add_action('rt_header', 'rt_header');


function rt_product_header()
{
    rt_get_template_part('product/header-nav');
}
add_action('rt_footer', 'rt_product_header');


function rt_product_button_action(){
    rt_get_template_part('product/button-action');
}
add_action('rt_footer', 'rt_product_button_action');
