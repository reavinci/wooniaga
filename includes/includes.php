<?php
require_once dirname(__FILE__) . '/class-template.php';
require_once dirname(__FILE__) . '/class-setup.php';
require_once dirname(__FILE__) . '/class-woocommerce.php';
require_once dirname(__FILE__) . '/class-menu.php';

require_once dirname(__FILE__) . '/inc-option.php';
require_once dirname(__FILE__) . '/inc-html.php';
require_once dirname(__FILE__) . '/inc-template-part.php';
require_once dirname(__FILE__) . '/inc-template-tag.php';