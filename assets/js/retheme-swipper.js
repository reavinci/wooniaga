var ProductGalleryThumbs = new Swiper(".product_gallery_thumbs", {
  spaceBetween: 10,
  slidesPerView: 4,
  freeMode: true,
  watchSlidesVisibility: true,
  watchSlidesProgress: true,
});

var ProductGallery = new Swiper(".product_gallery_display", {
  loop: true,

  // If we need pagination
  pagination: {
    el: ".swiper-pagination",
  },

  // Navigation arrows
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },

  // And if we need scrollbar
  scrollbar: {
    el: ".swiper-scrollbar",
  },

  thumbs: {
    swiper: ProductGalleryThumbs,
  },
});

const sliders = document.querySelectorAll(".swiper_slider");

Array.prototype.forEach.call(sliders, function (slider, index) {

  const options = slider.getAttribute('data-options');

  new Swiper(slider, JSON.parse(options));
  
});

jQuery(document).ready(function($) {
  $(document).on("found_variation", 'form.variations_form', function(event, variation) {
    ProductGalleryThumbs.slideTo(0, 700);
    ProductGallery.slideTo(0, 700);
  });
});