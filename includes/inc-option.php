<?php
/*==============================================
 * RT OPTION
==============================================
 *  @desc this function get option from customizer
 * if metabox not null this function return data form metabox
 * name customizer, option and metabox must same
 * role: filter > metabox > option > customizer
 */
function rt_option($setting, $default = '')
{
    // check default options
    $option = get_theme_mod($setting, $default);

    return apply_filters($setting, $option);

}
