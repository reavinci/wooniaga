<?php
define('RT_THEME_NAME', wp_get_theme()->get('Name'));
define('RT_THEME_SLUG', 'shobelio');
define('RT_THEME_DOMAIN', wp_get_theme()->get('TextDomain'));
define('RT_THEME_VERSION', wp_get_theme()->get('Version'));
define('RT_THEME_URL', wp_get_theme()->get('ThemeURI'));
define('RT_THEME_DOC', 'https: //www.panduan.webforia.id/shobelio');

// include all core file
require get_template_directory() . '/framework/framework.php';
require get_template_directory() . '/includes/includes.php';


