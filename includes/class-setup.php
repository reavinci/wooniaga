<?php
namespace Retheme;

class Setup
{
    public function __construct()
    {
        add_action('after_setup_theme', array($this, 'support'));
        add_action('wp_enqueue_scripts', array($this, 'styles'));

    }
    public function support()
    {
        add_theme_support('post-thumbnails');

        register_nav_menus(array(
            'primary' => 'Menu Primary',
            'mobile' => 'Menu Mobile',
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('post-thumbnails');
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        /**
         * Add theme support selective refresh for customizer
         */
        add_theme_support('customize-selective-refresh-widgets');

    }

    public function styles()
    {
        wp_enqueue_style('retheme-base', get_template_directory_uri() . '/assets/css/retheme-base.min.css', '', '1.4.0');
        
        //Swipper Js
        wp_enqueue_style('swipperjs', get_template_directory_uri() . '/assets/css/swiper-bundle.min.css', '', '1.4.0');
        wp_enqueue_script('swipperjs', get_template_directory_uri() . '/assets/js/swiper-bundle.min.js', '', '4.2.2', true);
        wp_enqueue_script('retheme-swipper', get_template_directory_uri() . '/assets/js/retheme-swipper.js', array('jquery', 'swipperjs'), '4.2.2', true);

    }
    
}

New Setup;