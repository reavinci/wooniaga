<?php
namespace Retheme;

class WooCommerce
{
    public function __construct()
    {
        add_action('after_setup_theme', [$this, 'setup']);
        add_filter('wp_enqueue_scripts', [$this, 'remove_default_scripts']);
        remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);
        add_action('wp_enqueue_scripts', array($this, 'styles'));
        add_filter('woocommerce_single_product_image_thumbnail_html', [$this, 'add_class_gallery_image'], 10, 2);

    }
    public function setup()
    {
        add_theme_support('woocommerce');
        add_theme_support('wc-product-gallery-lightbox');
    }

    public function remove_default_scripts($enqueue_styles)
    {
        ## Dequeue WooCommerce styles
        wp_dequeue_style('woocommerce-layout');
        wp_dequeue_style('woocommerce-general');
        wp_dequeue_style('woocommerce-smallscreen');
        wp_dequeue_style('wc-block-style');

        // ## Dequeue WooCommerce scripts
        wp_dequeue_script('wc-cart-fragments');
        wp_dequeue_script('woocommerce');
        wp_dequeue_script('wc-add-to-cart');
        wp_deregister_script('js-cookie');
        wp_dequeue_script('js-cookie');

    }

    public function styles()
    {
        wp_enqueue_script('retheme-variation', get_template_directory_uri() . '/assets/js/retheme-variation.js', array('jquery'), '4.2.2', true);

    }

    public function add_class_gallery_image($html, $attachment_id)
    {
        $html = str_replace('woocommerce-product-gallery__image', 'woocommerce-product-gallery__image swiper-slide', $html);
        return $html;
    }

}

new WooCommerce();
