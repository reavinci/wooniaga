<?php
namespace Retheme;

class Menu
{

    public function __construct()
    {
        // add_filter('nav_menu_css_class', array($this, 'menu_class'));
        add_filter('wp_nav_menu', array($this, 'submenu_class'));

    }
    /**
     * Add new menu class
     * add css class to menu item
     */
    public function menu_class($classes, $item)
    {
        if ($item->current) {
            $classes[] = "is-active";
        }

        $classes[] = "rt-menu__item";

        return $classes;

    }

    /**
     * add class sub menu
     * @category menu
     * @param [type] $menu
     * @return void
     */
    public function submenu_class($menu)
    {
        $menu = preg_replace('/ class="sub-menu"/', '/ class="sub-menu rt-menu__submenu" /', $menu);
        return $menu;
    }

    public function default_menu($args)
    {
        if (!current_user_can('manage_options')) {
            return;
        }

        // see wp-includes/nav-menu-template.php for available arguments
        extract($args);

        $link = $link_before
        . '<a href="' . admin_url('nav-menus.php') . '">' . wp_sprintf(__('%s Edit Menu %s', RT_THEME_DOMAIN), $before, $after) . '</a>'
            . $link_after;

        // We have a list
        if (false !== stripos($items_wrap, '<ul')
            or false !== stripos($items_wrap, '<ol')
        ) {
            $link = "<li class='rt-menu__item is-active'>$link</li>";
            $link .= wp_sprintf("<li class='rt-menu__item'><a>%s</a></li>", __('Sample Menu', RT_THEME_DOMAIN));
            $link .= wp_sprintf("<li class='rt-menu__item'><a>%s</a></li>", __('Sample Menu', RT_THEME_DOMAIN));
            $link .= wp_sprintf("<li class='rt-menu__item'><a>%s</a></li>", __('Sample Menu', RT_THEME_DOMAIN));
            $link .= wp_sprintf("<li class='rt-menu__item'><a>%s</a></li>", __('Sample Menu', RT_THEME_DOMAIN));
        }

        $output = sprintf($items_wrap, $menu_id, $menu_class, $link);
        if (!empty($container)) {
            $output = "<$container class='$container_class' id='$container_id'>$output</$container>";
        }

        if ($echo) {
            echo $output;
        }

        return $output;
    }

}

New Menu;